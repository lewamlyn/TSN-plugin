package org.plugin.tsnsched.views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class TableFrame extends JFrame {

	private JPanel contentPane;
	private JTable table;


	/**
	 * Create the frame.
	 */
	public TableFrame() {
        //headers for the table
        String[] columns = new String[] {
            "Queue", "100", "200", "300", "400","500", "600","700", "800"
        };
         
        //actual data for the table in a 2d array
        Object[][] data = new Object[][] {
            {1, false,true ,true ,true ,true, true,true,true},
            {2, true,false ,false ,false ,false, false,false,false},
            {3, true,false ,false ,false ,false, false,false,false},
            {4, false,true ,true ,true ,true, true,true,true},
            {5, false,true ,true ,true ,true, true,true,true},
            {6, false,true ,true ,true ,true, true,true,true},
            {7, true,true ,true ,true ,true, true,true,false},
            {8, true,true ,true ,true ,true, true,true,false}
        };
         
        final Class[] columnClass = new Class[] {
            Integer.class,Boolean.class, Boolean.class, Boolean.class,Boolean.class, Boolean.class, Boolean.class,Boolean.class, Boolean.class
        };
        //create table model with data
        DefaultTableModel model = new DefaultTableModel(data, columns) {
            @Override
            public boolean isCellEditable(int row, int column)
            {
                return false;
            }
            @Override
            public Class<?> getColumnClass(int columnIndex)
            {
                return columnClass[columnIndex];
            }
        };
         
        JTable table = new JTable(model);
         
        //add the table to the frame
        this.add(new JScrollPane(table));
         
        this.setTitle("Table Example");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);       
        this.pack();
        this.setVisible(true);		
	}
}
