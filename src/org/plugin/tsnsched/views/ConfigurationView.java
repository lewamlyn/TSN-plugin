package org.plugin.tsnsched.views;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.filebuffers.FileBuffers;
import org.eclipse.core.filebuffers.ITextFileBuffer;
import org.eclipse.core.filebuffers.ITextFileBufferManager;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.part.ViewPart;
import org.omnetpp.inifile.editor.model.IReadonlyInifileDocument;
import org.omnetpp.inifile.editor.model.InifileAnalyzer;
import org.omnetpp.inifile.editor.model.InifileDocument;
import org.omnetpp.inifile.editor.model.InifileUtils;
import org.omnetpp.inifile.editor.model.SectionKey;
import org.omnetpp.inifile.editor.model.Timeout;
import org.omnetpp.ned.core.NedResourcesPlugin;
import org.omnetpp.ned.model.INedElement;
import org.omnetpp.ned.model.NedElement;
import org.omnetpp.ned.model.NedElementConstants;
import org.omnetpp.ned.model.ex.CompoundModuleElementEx;
import org.omnetpp.ned.model.ex.ConnectionElementEx;
import org.omnetpp.ned.model.ex.NedFileElementEx;
import org.omnetpp.ned.model.ex.SubmoduleElementEx;
import org.omnetpp.ned.model.pojo.NedElementTags;
import org.omnetpp.ned.model.pojo.NedFileElement;
import org.plugin.model.SimplifiedInifileDocument;
import org.plugin.model.TreeContentProvider;
import org.plugin.model.TreeLabelProvider;
import org.plugin.model.XMLNode;

import static org.eclipse.swt.events.SelectionListener.widgetSelectedAdapter;

/**
 * This sample class demonstrates how to plug-in a new
 * workbench view. The view shows data obtained from the
 * model. The sample creates a dummy model on the fly,
 * but a real implementation would connect to the model
 * available either in this or another plug-in (e.g. the workspace).
 * The view is connected to the model using a content provider.
 * <p>
 * The view uses a label provider to define how model
 * objects should be presented in the view. Each
 * view can present the same model objects using
 * different labels and icons, if needed. Alternatively,
 * a single label provider can be shared between views
 * in order to ensure that objects of the same type are
 * presented in the same way everywhere.
 * <p>
 */

public class ConfigurationView extends ViewPart {

	/**
	 * The ID of the view as specified by the extension.
	 */
	public static final String ID = "org.plugin.tsnsched.views.ConfigurationView";
	protected TreeViewer viewer;
	public String filepath;

	
	
	

	public ConfigurationView() {
	}

	public void createPartControl(Composite parent) {

	    viewer = new TreeViewer(parent); 
	    viewer.setLabelProvider(new TreeLabelProvider()); 
	    viewer.setContentProvider(new TreeContentProvider()); 
	    viewer.setInput(null);
	    
	    
	    makeActions();
	    contributeToActionBars();
	}

		 	
	private void contributeToActionBars() {
		IActionBars bars = getViewSite().getActionBars();
		fillLocalToolBar(bars.getToolBarManager());
	}

	private void fillLocalToolBar(IToolBarManager manager) {
		//manager.add(action1);

	}
	
	
	private void makeActions() {

	}


	
	
	public void readINIFile(){
	    try {
			File inputFileini = new File(org.plugin.tsnsched.Activator.filepath);
			File inputFilened = new File(org.plugin.tsnsched.Activator.filepath.replace(".ini", ".ned"));
			IWorkspace workspace= ResourcesPlugin.getWorkspace();
			
			IPath pathini = org.eclipse.core.runtime.Path.fromOSString(inputFileini.getAbsolutePath());
			IPath pathned = org.eclipse.core.runtime.Path.fromOSString(inputFilened.getAbsolutePath());
			
			IFile iFileini = workspace.getRoot().getFileForLocation(pathini);
			IFile iFilened = workspace.getRoot().getFileForLocation(pathned);
			
			SimplifiedInifileDocument doc = new SimplifiedInifileDocument(iFileini);
			
			

			
			
			IEditorPart editorPart = getSite().getPage().getActiveEditor();
			
			
			String paramFullPath = "workstation1";
			Boolean hasNedDefault = true;
			String[] sectionChain = {"General"};
			
			IDocument idoc;
			
			ITextFileBufferManager bufferManager = FileBuffers.getTextFileBufferManager();
			
			try {
				bufferManager.connect(pathini, null);
				ITextFileBuffer textFileBuffer = bufferManager.getTextFileBuffer(pathini);
				idoc = textFileBuffer.getDocument();
				InifileDocument d = new InifileDocument(idoc, iFileini);
				d.parse();
				

				NedFileElement nedfileelement = (NedFileElement)NedResourcesPlugin.getNedResources().getNedFileElement(iFilened);

				List<INedElement> children = nedfileelement.getChildren();
				String output = nedfileelement.getAttribute(0);
				
				CompoundModuleElementEx compound = (CompoundModuleElementEx)children.get(children.size()-1);
				
				XMLNode root = new XMLNode("");
				
				List<INedElement> temp;
				temp = compound.getChildrenWithTag(NedElementTags.NED_SUBMODULES).get(0).getChildren();
				List<SubmoduleElementEx> submodules = new ArrayList<SubmoduleElementEx>();
				for (INedElement iNedElement : temp) {
					submodules.add((SubmoduleElementEx)iNedElement);
					String name = ((SubmoduleElementEx)iNedElement).getName();
					root.add(new XMLNode(name));
					
				}
				
				temp = compound.getChildrenWithTag(NedElementTags.NED_CONNECTIONS).get(0).getChildren();
				List<ConnectionElementEx> connections = new ArrayList<ConnectionElementEx>();
				for (INedElement iNedElement : temp) {
					connections.add((ConnectionElementEx)iNedElement);
					// Find for one direction first
					String source = ((ConnectionElementEx)iNedElement).getDestModule();
					String destination = ((ConnectionElementEx)iNedElement).getSrcModule();
					root.getByName(source).add(new XMLNode(destination));
					// Does it go both ways?
					if(((ConnectionElementEx)iNedElement).getIsBidirectional()){
						// Add the other direction also
						root.getByName(destination).add(new XMLNode(source));
					}
					
				}
				viewer.setInput(root);
				
				
				//ISubmoduleOrConnection element = new ISubmoduleOrConnection();
				//String activeSection;
                InifileAnalyzer analyzer = new InifileAnalyzer(d);
                
                Timeout timeout = new Timeout(10000);
				
                INedElement model = nedfileelement.getModel();
                
				
				//InifileUtils.resolveLikeExpr(moduleFullPath, children.get(0), sectionChain[0], analyzer, (IReadonlyInifileDocument)d, timeout);
				
				String network = InifileUtils.lookupNetwork((IReadonlyInifileDocument)d,"General");
				
				
				List<SectionKey> l = InifileUtils.lookupParameter(paramFullPath, hasNedDefault, sectionChain, (IReadonlyInifileDocument)d);
				
				doc.parse();

				
			}catch(Exception e){

				XMLNode root = new XMLNode("");
				root.add(new XMLNode("ERROR : " + e.toString()));
			}
			
			
			
			
			
			
			
	
			
		
			
			


	   }
	   catch(Exception e){

	   }
	}
	
	private void displayEditPromt(XMLNode node){
		int numOfAttributes = node.attributes.size();
		Display display = PlatformUI.getWorkbench().getDisplay();
		ArrayList<Label> Labels = new ArrayList<>();
		ArrayList<Text> Texts = new ArrayList<>();
		
		
		final Shell dialog = new Shell (display, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
		dialog.setText("Dialog Shell");
		FormLayout formLayout = new FormLayout ();
		formLayout.marginWidth = 10;
		formLayout.marginHeight = 10;
		formLayout.spacing = 10;
		dialog.setLayout (formLayout);

		FormData data = new FormData ();
		Button cancel = new Button (dialog, SWT.PUSH);
		cancel.setText ("Cancel");
		data.width = 60;
		data.right = new FormAttachment (100, 0);
		data.bottom = new FormAttachment (100, 0);
		cancel.setLayoutData (data);
		cancel.addSelectionListener (widgetSelectedAdapter(event -> {

			dialog.close ();
		}));
		
		Button ok = new Button (dialog, SWT.PUSH);
		ok.setText ("OK");
		data = new FormData ();
		data.width = 60;
		data.right = new FormAttachment (cancel, 0, SWT.DEFAULT);
		data.bottom = new FormAttachment (100, 0);
		ok.setLayoutData (data);
		ok.addSelectionListener (widgetSelectedAdapter(event -> {

			node.string = Texts.get(Texts.size() - 1).getText(); // Change the node based on user input
			for (int i = 0; i < numOfAttributes; i++) {
				node.attributesvalues.set(numOfAttributes - i - 1, Texts.get(i).getText());
			}
			dialog.close ();
		}));
		
		for (int i = 0; i < numOfAttributes + 1; i++) {
			//Text
			Text text = new Text (dialog, SWT.BORDER);
			data = new FormData ();
			data.width = 200;
			data.right = new FormAttachment (100, 0);
			if(i==0)
				data.bottom = new FormAttachment (cancel, 0,SWT.TOP);
			else
				data.bottom = new FormAttachment (Texts.get(i-1), 0,SWT.DEFAULT);
			text.setLayoutData (data);
			// Populate with text
			if(i == numOfAttributes){
				// This is the last, add node name/text
				text.setText(node.string);
			}
			else{
				// Add attributes in reveres order
				text.setText(node.attributesvalues.get(numOfAttributes - i - 1));
			}
			
			Texts.add(text);
			
			// Label
			Label label = new Label (dialog, SWT.NONE);
			data = new FormData ();
			data.left = new FormAttachment(0, 0);
			data.right = new FormAttachment (text, 0, SWT.LEFT);
			data.bottom = new FormAttachment (text, 0, SWT.CENTER);
			data.top = new FormAttachment (text, 0, SWT.CENTER);
			label.setLayoutData (data);
			if(i == numOfAttributes){
				// This is the last, add node name/text
				label.setText("Node :");
			}
			else{
				// Add attributes in reveres order
				label.setText(node.attributes.get(numOfAttributes - i - 1)+ " :");
			}
			Labels.add(label);
		}
		
		dialog.setDefaultButton (ok);
		dialog.pack ();
		dialog.open ();

		while (!dialog.isDisposed ()) {
			if (!display.readAndDispatch ()) display.sleep ();
		}
		
	}

	/**
	 * Passing the focus request to the viewer's control.
	 */
	public void setFocus() {
		viewer.getControl().setFocus();
	}
}