package org.plugin.model;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.dnd.DragSource;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTarget;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Scale;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;

public class FlowListEntry2 extends Composite {
	private Composite composite;
	private Label lblQueue,lblPath,lblRequirements;
	private Combo cboQueue;
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	private Label lblNewLabel_2;
	private Label lblNewLabel_3;
	private SashForm sashForm_1;
	private Text text_2;
	private Group grpSize;
	private Text text_3;
	private Label lblBytes;
	private Group grpStartDelay;
	private Text text_4;
	private Label lblMs_2;
	public Button btnRemove;

	/**
	 * Create the composite.
	 * @param parent
	 * @param style
	 */
	public FlowListEntry2(Composite parent, int style) {
		super(parent, SWT.BORDER);
		setBackground(SWTResourceManager.getColor(SWT.COLOR_RED));
		addDisposeListener(new DisposeListener() {
			public void widgetDisposed(DisposeEvent e) {
				toolkit.dispose();
			}
		});
		toolkit.adapt(this);
		toolkit.paintBordersFor(this);
		
		
		
		GridData gd_composite = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_composite.widthHint = 497;
		this.setLayoutData(gd_composite);
		this.setSize(660, 160);

		this.setLayout(new FormLayout());
		
		lblQueue = new Label(this, SWT.NONE);
		lblQueue.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblQueue.setAlignment(SWT.CENTER);
		FormData fd_lblQueue = new FormData();
		fd_lblQueue.right = new FormAttachment(0, 67);
		lblQueue.setLayoutData(fd_lblQueue);
		lblQueue.setText("Queue: ");
		
		
		
		
		lblPath = new Label(this, SWT.NONE);
		fd_lblQueue.left = new FormAttachment(lblPath, 0, SWT.LEFT);
		lblPath.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		FormData fd_lblPath = new FormData();
		fd_lblPath.top = new FormAttachment(0, 10);
		fd_lblPath.left = new FormAttachment(0, 10);
		lblPath.setLayoutData(fd_lblPath);

		lblPath.setText("Path:");
		
		lblRequirements = new Label(this, SWT.NONE);
		lblRequirements.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		FormData fd_lblNewLabel = new FormData();
		fd_lblNewLabel.bottom = new FormAttachment(lblQueue, -22);
		fd_lblNewLabel.left = new FormAttachment(lblQueue, 0, SWT.LEFT);
		lblRequirements.setLayoutData(fd_lblNewLabel);

		lblRequirements.setText("Requirements:");
		
		cboQueue = new Combo(this, SWT.NONE);
		fd_lblQueue.top = new FormAttachment(cboQueue, -2, SWT.TOP);
		cboQueue.setItems(new String[] {"1", "2", "3", "4", "5", "6", "7", "8"});
		FormData fd_combo = new FormData();
		fd_combo.bottom = new FormAttachment(100);
		fd_combo.left = new FormAttachment(lblQueue, 6);
		cboQueue.setLayoutData(fd_combo);

		cboQueue.select(7);
		
		toolkit.adapt(lblQueue, true, true);
		toolkit.adapt(lblPath, true, true);
		toolkit.adapt(lblRequirements, true, true);
		toolkit.adapt(cboQueue);
		toolkit.paintBordersFor(cboQueue);
		toolkit.adapt(lblRequirements, true, true);
		
		SashForm sashForm = new SashForm(this, SWT.NONE);
		fd_lblNewLabel.top = new FormAttachment(sashForm, 21);
		FormData fd_sashForm = new FormData();
		fd_sashForm.left = new FormAttachment(0, 53);
		fd_sashForm.top = new FormAttachment(0, 8);
		sashForm.setLayoutData(fd_sashForm);
		toolkit.adapt(sashForm);
		toolkit.paintBordersFor(sashForm);
		
		Label lblNewLabel = new Label(sashForm, SWT.BORDER);
		lblNewLabel.setAlignment(SWT.CENTER);
		lblNewLabel.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		toolkit.adapt(lblNewLabel, true, true);
		lblNewLabel.setText("Start");
		
		Label lblNewLabel_1 = new Label(sashForm, SWT.BORDER);
		lblNewLabel_1.setAlignment(SWT.CENTER);
		lblNewLabel_1.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		toolkit.adapt(lblNewLabel_1, true, true);
		lblNewLabel_1.setText("Switch");
		
		lblNewLabel_2 = new Label(sashForm, SWT.BORDER);
		lblNewLabel_2.setAlignment(SWT.CENTER);
		lblNewLabel_2.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblNewLabel_2.setText("Switch");
		toolkit.adapt(lblNewLabel_2, true, true);
		
		lblNewLabel_3 = new Label(sashForm, SWT.BORDER);
		lblNewLabel_3.setAlignment(SWT.CENTER);
		lblNewLabel_3.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblNewLabel_3.setText("Destination");
		toolkit.adapt(lblNewLabel_3, true, true);
		
		Button btnNewButton = new Button(this, SWT.NONE);
		fd_sashForm.right = new FormAttachment(100, -73);
		FormData fd_btnNewButton = new FormData();
		fd_btnNewButton.right = new FormAttachment(100, -3);
		fd_btnNewButton.top = new FormAttachment(0, 8);
		btnNewButton.setLayoutData(fd_btnNewButton);
		btnNewButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		toolkit.adapt(btnNewButton, true, true);
		btnNewButton.setText("Add node");
		
		sashForm_1 = new SashForm(this, SWT.NONE);
		FormData fd_sashForm_1 = new FormData();
		fd_sashForm_1.top = new FormAttachment(btnNewButton, 2);
		sashForm.setWeights(new int[] {1, 1, 1, 1});
		fd_sashForm_1.left = new FormAttachment(lblRequirements, 6);
		fd_sashForm_1.right = new FormAttachment(100, -10);
		sashForm_1.setLayoutData(fd_sashForm_1);
		toolkit.adapt(sashForm_1);
		toolkit.paintBordersFor(sashForm_1);
		
		Group grpCyclePeriod = new Group(sashForm_1, SWT.NONE);
		grpCyclePeriod.setText("Cycle period");
		toolkit.adapt(grpCyclePeriod);
		toolkit.paintBordersFor(grpCyclePeriod);
		grpCyclePeriod.setLayout(new FormLayout());
		
		text_2 = new Text(grpCyclePeriod, SWT.BORDER);
		FormData fd_text_2 = new FormData();
		fd_text_2.top = new FormAttachment(0, 3);
		fd_text_2.left = new FormAttachment(0, 3);
		text_2.setLayoutData(fd_text_2);
		toolkit.adapt(text_2, true, true);
		
		Label lblMs = new Label(grpCyclePeriod, SWT.NONE);
		fd_text_2.right = new FormAttachment(lblMs, -6);
		FormData fd_lblMs = new FormData();
		fd_lblMs.top = new FormAttachment(0, 6);
		fd_lblMs.right = new FormAttachment(100, -10);
		lblMs.setLayoutData(fd_lblMs);
		toolkit.adapt(lblMs, true, true);
		lblMs.setText("us");
		
		grpSize = new Group(sashForm_1, SWT.NONE);
		grpSize.setText("Size");
		toolkit.adapt(grpSize);
		toolkit.paintBordersFor(grpSize);
		grpSize.setLayout(new FormLayout());
		
		text_3 = new Text(grpSize, SWT.BORDER);
		FormData fd_text_3 = new FormData();
		fd_text_3.top = new FormAttachment(0, 3);
		fd_text_3.left = new FormAttachment(0, 3);
		text_3.setLayoutData(fd_text_3);
		toolkit.adapt(text_3, true, true);
		
		lblBytes = new Label(grpSize, SWT.NONE);
		fd_text_3.right = new FormAttachment(lblBytes, -6);
		lblBytes.setText("Bytes");
		FormData fd_lblBytes = new FormData();
		fd_lblBytes.top = new FormAttachment(0, 6);
		fd_lblBytes.right = new FormAttachment(100, -10);
		lblBytes.setLayoutData(fd_lblBytes);
		toolkit.adapt(lblBytes, true, true);
		
		grpStartDelay = new Group(sashForm_1, SWT.NONE);
		grpStartDelay.setText("Start delay");
		toolkit.adapt(grpStartDelay);
		toolkit.paintBordersFor(grpStartDelay);
		grpStartDelay.setLayout(new FormLayout());
		
		text_4 = new Text(grpStartDelay, SWT.BORDER);
		FormData fd_text_4 = new FormData();
		fd_text_4.left = new FormAttachment(0, 3);
		fd_text_4.top = new FormAttachment(0, 3);
		text_4.setLayoutData(fd_text_4);
		toolkit.adapt(text_4, true, true);
		
		lblMs_2 = new Label(grpStartDelay, SWT.NONE);
		fd_text_4.right = new FormAttachment(lblMs_2, -12);
		lblMs_2.setText("us");
		FormData fd_lblMs_2 = new FormData();
		fd_lblMs_2.top = new FormAttachment(text_4, 3, SWT.TOP);
		fd_lblMs_2.right = new FormAttachment(100, -10);
		lblMs_2.setLayoutData(fd_lblMs_2);
		toolkit.adapt(lblMs_2, true, true);
		sashForm_1.setWeights(new int[] {1, 1, 1, 1, 1, 1, 1});
		
		btnRemove = new Button(this, SWT.NONE);
		fd_sashForm_1.bottom = new FormAttachment(btnRemove, -18);
		btnRemove.setSelection(true);
		FormData fd_btnRemove = new FormData();
		fd_btnRemove.bottom = new FormAttachment(100, -2);
		fd_btnRemove.right = new FormAttachment(btnNewButton, 0, SWT.RIGHT);
		btnRemove.setLayoutData(fd_btnRemove);
		toolkit.adapt(btnRemove, true, true);
		btnRemove.setText("Remove");

	}
}
