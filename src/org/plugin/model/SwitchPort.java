package org.plugin.model;

import java.util.ArrayList;
import java.util.List;

public class SwitchPort implements Cloneable {
	// 0..x Switches || 0..8 ports || 8 queues 
	// List timePeriods 200us, 300us
	// List queue Setup [1111110]
	public List<SwitchEntry> entryList;
	int port;
	String connectedTo;
	int cycle=0;
	public void setCycleFromXMLNode(int c){
		cycle = c;
		
	}

	public SwitchPort(int port_id, String connectedNode) {
		entryList = new ArrayList<SwitchEntry>();
		port = port_id;
		connectedTo = connectedNode;
		
	}
	public void AddEntry(SwitchEntry entry){
		entryList.add(entry);
	}
	public String getID() {
		return Integer.toString(port);
	}
	public String getConnectedNode() {
		return connectedTo;
	}
	public List<SwitchEntry> getEntries() {
		return entryList;
		
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		SwitchPort t = (SwitchPort)super.clone();
		
		t.entryList = new ArrayList<SwitchEntry>();
		t.entryList.addAll(this.entryList);
		
		return t;
	}

}
